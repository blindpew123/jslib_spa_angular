import {FormState} from './form-state';
import {ActivatedRoute, Router} from '@angular/router';
import {MatBottomSheet, MatBottomSheetRef} from '@angular/material';
import {MAT_BOTTOM_SHEET_DATA} from '@angular/material';
import {Component, Inject} from '@angular/core';
import {BottomSheetComponent} from './bottom-sheet/bottom-sheet.component';


export class MultiPurposeForm {
  state: FormState;
  ComponentFormState = FormState;
  constructor(protected route: ActivatedRoute, protected router: Router, protected bottomSheet: MatBottomSheet) {}

  protected setFormState(): void {
    this.route.data.subscribe((data) => {
      if (data['mode'] === 'add') {
        this.state = FormState.Add;
      } else {
        this.state = FormState.Search;
      }
    });
  }
/*
  protected openBottomSheet(data: any): void {
     this.bottomSheet.open(BottomSheetComponent, data);
  } */
}
/*
@Component({
  selector: 'app-bottom-sheet',
  templateUrl: 'app-bottom-sheet.html'
})
export class BottomSheetComponent {
  objDescription: string;
  constructor(private bottomSheetRef: MatBottomSheetRef<BottomSheetComponent>, @Inject(MAT_BOTTOM_SHEET_DATA) public data: any) {
    this.buildInfoMessage(data.obj);
  }

  private buildInfoMessage(obj: any): void {
    if (!!obj) {
      this.objDescription = 'Добавлено успешно:' + this.getObjDescription(obj);
    } else {
      this.objDescription = 'Произошла ошибка, попробуйте снова';
    }
  }

  private getObjDescription<T>(obj: T): string {
    const objectKeys = Object.keys(obj) as Array<keyof T>;
    let result = '';
    for (const key of objectKeys) {
      if (key === 'id') {
        continue;
      }
      if (obj[key] instanceof Object) {
        result += this.getObjDescription(obj[key]);
      } else {
        result += obj[key];
        result += ' ';
      }
    }
    return result;
  }
}
*/
