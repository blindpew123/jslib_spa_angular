import { Injectable } from '@angular/core';
import {Observable, of} from 'rxjs';
import {Student} from './Entity/student';
import {Grade} from './Entity/grade';
import {catchError, tap} from 'rxjs/operators';
import {HttpClient} from '../../node_modules/@angular/common/http';
import {JsLibService} from './js-lib-service';
import {MatBottomSheet} from '@angular/material';


@Injectable({
  providedIn: 'root'
})
export class StudentService extends JsLibService {

  constructor(private http: HttpClient, protected bottomSheet: MatBottomSheet) {
    super(bottomSheet);
  }

  getStudents(): Observable<Student[]> {
    return this.http.get<Student[]>(this.jsLibUrl + '/allStudents')
      .pipe( tap(students => {
      //  console.log(students);
      //  this.handleSuccess('Список учеников загружен');
        }),
        catchError(this.handleError('Не удалось получить список учеников', [])));
  }

  getStudent(id: number): Observable<Student> {
    const url = `${this.jsLibUrl}/getStudent/${id}`;
    return this.http.get<Student>(url)
      .pipe( tap(student => {
   //     console.log(student);
   //     this.handleSuccess('Получены данные ученика: ', student);
        }),
        catchError(this.handleError<any>('Не удалось получить данные об ученике с id ' + id)));
  }

  getStudentFromIssue(issueId: number): Observable<Student> {
    const url = `${this.jsLibUrl}/getStudentFromIssue/${issueId}`;
    return this.http.get<Student>(url)
      .pipe( tap(student => {
          //     console.log(student);
          //     this.handleSuccess('Получены данные ученика: ', student);
        }),
        catchError(this.handleError<any>(' Не удалось получить данные об ученике из записи о выдаче с id ' + issueId)));
  }

  addStudent(student: Student): Observable<Student> {
    return this.http.post<Student>(this.jsLibUrl + `/addStudent`, JSON.stringify(student), JsLibService.httpOptions).pipe(
      tap((addedStudent: Student) => {
     //   console.log(`Added student with id=${addedStudent.id}`);
        this.handleSuccess('Запись об ученике успешно добавлена: ', student);
      }),
      catchError(this.handleError<Student>('Не удалось добавить ученика'))
    );
  }

  findStudents(student: Student): Observable<Student[]> {
    return this.http.post<Student>(this.jsLibUrl + `/getStudentsByTemplate`, JSON.stringify(student), JsLibService.httpOptions).pipe(
      tap((findedStudents: Student[]) => {
        console.log(findedStudents),
          this.handleSuccess('Поиск учеников успшно завершен. Найдено ' +
            findedStudents.length +
            this.getRightWord(findedStudents.length));
      }),

      catchError(this.handleError<Student[]>('Ошибка поиска ученика', []))
    );
  }


  getEmptyStudent(): Student {
    const student = new Student();
    const grade = new Grade();
    student.grade = grade;
    return student;
  }
}
