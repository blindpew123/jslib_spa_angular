import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {Book} from '../Entity/book';
import {ActivatedRoute} from '@angular/router';
import {BookService} from '../book.service';
import { Location } from '@angular/common';
import {ChoosenOneStudentComponent} from '../choosen-one-student/choosen-one-student.component';

@Component({
  selector: 'app-book-detail',
  templateUrl: './book-detail.component.html',
  styleUrls: ['./book-detail.component.css']
})
export class BookDetailComponent implements OnInit {
  @ViewChild(ChoosenOneStudentComponent)
  private choosenStudent: ChoosenOneStudentComponent;
  notReturned: boolean;
  debt: number;
  book: Book;
  wasIssued: string;
  issueId: number;

  constructor(private route: ActivatedRoute,
              private bookService: BookService,
              private location: Location) { }

  ngOnInit() {
    this.getBook();
  }

  private getBook(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.bookService.getBook(id).subscribe(book => {
      this.processBook(book);
    });
  }

  private processBook(book: Book): void {
    this.book = book;
    this.notReturned = this.bookService.notReturned(book);
    if (this.notReturned) {
      this.debt = this.bookService.calculateDebt(book);
      this.issueId = book.bookIssues[book.bookIssues.length - 1].id;
      this.wasIssued = book.bookIssues[book.bookIssues.length - 1].issueDateTime;
    }
  }

  addBook() {
    this.choosenStudent.addBook(this.book);
  }

  returnBook() {
    this.bookService.returnBook(this.book).subscribe(returnedBook => {
      this.processBook(returnedBook);
    });
  }

  goBack(): void {
    this.location.back();
  }

}
