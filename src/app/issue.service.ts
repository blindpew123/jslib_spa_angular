import { Injectable } from '@angular/core';
import {JsLibService} from './js-lib-service';
import {MatBottomSheet} from '@angular/material';
import {HttpClient} from '../../node_modules/@angular/common/http';
import {Observable} from 'rxjs';
import {catchError, tap} from 'rxjs/operators';
import {Issue} from './Entity/issue';

@Injectable({
  providedIn: 'root'
})
export class IssueService extends JsLibService {

  constructor(private http: HttpClient , protected bottomSheet: MatBottomSheet) {
    super(bottomSheet);
  }

  addIssues (issues: {studentId: number, bookId: number}[]): Observable<{studentId: number, bookId: number}[]> {
    return this.http.post<{studentId: number, bookId: number}[]>(
      this.jsLibUrl + `/addIssues`, JSON.stringify(issues), JsLibService.httpOptions).pipe(
                tap((resultIssues: {studentId: number, bookId: number}[]) => {
                  //   console.log(`Received issues: ${issues}`);
                  this.handleSuccess('Все книги успешно выданы');
                }),
                catchError(this.handleError<{studentId: number, bookId: number}[]>('Не удалось выдать книги ученику', [] ))
    );
  }

  updateIssue (issue: Issue): Observable<Issue> {
    return this.http.put<Issue>(this.jsLibUrl + `/updateIssue`, JSON.stringify(issue), JsLibService.httpOptions).pipe(
      tap((resultIssue: Issue) => {
        //   console.log(`Updated issue: ${issue}`);
        this.handleSuccess('Книга возвращена в библиотеку');
      }),
      catchError(this.handleError<Issue>('Не удалось вернуть книгу'))
    );
  }
}
