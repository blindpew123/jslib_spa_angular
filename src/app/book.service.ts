import { Injectable } from '@angular/core';
import {Observable, of} from 'rxjs';
import {Book} from './Entity/book';
import {catchError, tap} from 'rxjs/operators';
import {Author} from './Entity/author';
import {Genre} from './Entity/genre';
import {Country} from './Entity/country';
import {HttpClient} from '@angular/common/http';
import {JsLibService} from './js-lib-service';
import {MatBottomSheet} from '@angular/material';



@Injectable({
  providedIn: 'root'
})
export class BookService extends JsLibService {

  constructor(private http: HttpClient, protected bottomSheet: MatBottomSheet) {
    super(bottomSheet);
  }

  getBooks(): Observable<Book[]> {
    return this.http.get<Book[]>(this.jsLibUrl + '/allBooks')
      .pipe( tap(books => {
        console.log(books)
       // this.handleSuccess('Список книг загружен');
      }),
        catchError(this.handleError('Ошибка загузки списка книг', [])));
  }

  getBook(id: number): Observable<Book> {
    const url = `${this.jsLibUrl}/getBook/${id}`;
    return this.http.get<Book>(url)
      .pipe( tap(book => {
       // console.log(book),
       // this.handleSuccess('Информация о книге загружена: ', book);
        }),
        catchError(this.handleError<any>('Ошибка загрузки информации о книге с id ' + id)));
  }

  addBook(book: Book): Observable<Book> {
    return this.http.post<Book>(this.jsLibUrl + `/addBook`, JSON.stringify(book), JsLibService.httpOptions).pipe(
      tap((addedBook: Book) => {
        // console.log(`added book with id=${addedBook.id}`),
          this.handleSuccess('Книга добавлена:', addedBook);
      }),
      catchError(this.handleError<Book>('Ошибка добавления книги'))
    );
  }

  returnBook(book: Book): Observable<Book> {
    return this.http.post<Book>(this.jsLibUrl + `/returnBook`, JSON.stringify(book), JsLibService.httpOptions).pipe(
      tap((returnedBook: Book) => {
        // console.log(`added book with id=${returnedBook.id}`),
        this.handleSuccess('Книга возвращена в библиотеку:');
      }),
      catchError(this.handleError<Book>('Ошибка возврата книги'))
    );
  }

  findBooks(book: Book): Observable<Book[]> {
    return this.http.post<Book>(this.jsLibUrl + `/getBooksByTemplate`, JSON.stringify(book), JsLibService.httpOptions).pipe(
      tap((findedBook: Book[]) => {
       // console.log(findedBook);
        this.handleSuccess('Поиск книг успешно завершен. Найдено ' +
          findedBook.length +
          this.getRightWord(findedBook.length));
      }),
      catchError(this.handleError<Book[]>('Ошибка поиска книг', []))
    );
  }

  notReturned(book: Book): boolean {
    return !!book.bookIssues && !!book.bookIssues.length && !book.bookIssues[book.bookIssues.length - 1].returnDateTime;
  }

  calculateDebt(book: Book): number {
    if (!book || !book.bookIssues.length || !!book.bookIssues[book.bookIssues.length - 1].returnDateTime) {
      return 0;
    }
    const issued: Date = new Date(book.bookIssues[book.bookIssues.length - 1].issueDateTime);
    const now: Date = new Date();
    if (now.getFullYear() !== issued.getFullYear()) {
      const oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
      const diffDays = Math.round(Math.abs((now.getTime() - issued.getTime()) / (oneDay)));
      return diffDays * 10;
    }
    return 0;
  }
  getEmptyBook(): Book {
    const author = new Author();
    const genre = new Genre();
    const country = new Country();
    const book = new Book();
    book.author = author;
    book.country = country;
    book.genre = genre;
    return book;
  }
}

