import {Component, OnDestroy, OnInit} from '@angular/core';
import {Book} from '../Entity/book';
import {BookService} from '../book.service';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.css']
})
export class BookListComponent implements OnInit, OnDestroy {
  books: Book[];
  navigationSubscription;

  constructor(public bookService: BookService, private route: ActivatedRoute, private router: Router) {
      this.navigationSubscription = this.router.events.subscribe((e: any) => {
      // If it is a NavigationEnd event re-initialise the component
      if (e instanceof NavigationEnd) {
        this.initializeList();
      }
    });
  }
  ngOnDestroy(): void {
    // avoid memory leaks here by cleaning up after ourselves. If we
    // don't then we will continue to run our initialiseInvites()
    // method on every navigationEnd event.
    if (this.navigationSubscription) {
      this.navigationSubscription.unsubscribe();
    }
  }

  private initializeList(): void {
  this.route.queryParams.subscribe(params => {
      if (!!params['books']) {
        this.books = JSON.parse(params['books']);
      } else {
        this.getBooks();
      }
    });
  }

  ngOnInit() {
  }

  getBooks(): void {
    this.bookService.getBooks().subscribe(books => this.books = books );
  }



}
