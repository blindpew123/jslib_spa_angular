import {Component, OnInit, ViewChild} from '@angular/core';
import {Student} from '../Entity/student';
import {ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';
import {StudentService} from '../student.service';
import {ChoosenOneStudentComponent} from '../choosen-one-student/choosen-one-student.component';


@Component({
  selector: 'app-student-detail',
  templateUrl: './student-detail.component.html',
  styleUrls: ['./student-detail.component.css']
})
export class StudentDetailComponent implements OnInit {

  @ViewChild(ChoosenOneStudentComponent)
  private choosenStudent: ChoosenOneStudentComponent;
  student: Student;

  constructor(private route: ActivatedRoute,
              private studentService: StudentService,
              private location: Location) { }

  ngOnInit() {
    this.getStudent();
  }

  getStudent(): void {
    if (this.route.snapshot.paramMap.has('id')) {
      const id = +this.route.snapshot.paramMap.get('id');
      this.studentService.getStudent(id).subscribe(student => this.student = student);
    } else {
      const issueId = +this.route.snapshot.paramMap.get('issue');
      this.studentService.getStudentFromIssue(issueId).subscribe(student => this.student = student);
    }
  }

  selectStudent() {
    this.choosenStudent.setChoosenStudent(this.student);
  }

  goBack(): void {
    this.location.back();
  }
}
