import {Component, Inject, OnInit} from '@angular/core';
import {MAT_BOTTOM_SHEET_DATA, MatBottomSheetRef} from '@angular/material';


/**
 *  Component for informing the user of short messages at the bottom of the screen
 *
 */


@Component({
  selector: 'app-bottom-sheet',
  templateUrl: 'bottom-sheet.component.html'
})
export class BottomSheetComponent {

  objDescription: string;
  isError: boolean;
  message: string;
  /** @param bottomSheetRef : link to MatBottomSheet according Angular Material Doc
  *   @param data : object for displaying message.
  *   Possible fields are: 'obj' - must contain object, which fields will be shown in message
  *                        'error' - error message
  *                        'info' - simple info message
  */

  constructor(private bottomSheetRef: MatBottomSheetRef<BottomSheetComponent>, @Inject(MAT_BOTTOM_SHEET_DATA) public data: any) {
    this.buildInfoMessage(data);
  }

  private buildInfoMessage(obj: any): void {
    console.log(obj);
    this.isError = !!obj.error;
    if (this.isError) {
      this.message = obj.error.message;
    } else {
      if (!!obj) {
        this.objDescription = this.getObjDescription(obj.obj);
      }
      this.message = obj.info + this.objDescription;
    }
  }

  private getObjDescription<T>(obj: T): string {
    const objectKeys = Object.keys(obj) as Array<keyof T>;
    let result = '';
    for (const key of objectKeys) {
      if (key === 'id') {
        continue;
      }
      if (obj[key] instanceof Object) {
        result += this.getObjDescription(obj[key]);
      } else {
        result += obj[key];
        result += ' ';
      }
    }
    return result;
  }
}
