import {HttpHeaders} from '../../node_modules/@angular/common/http';
import {Observable, of} from 'rxjs';
import {MatBottomSheet} from '@angular/material';
import {BottomSheetComponent} from './bottom-sheet/bottom-sheet.component';

export class JsLibService {
  protected static httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  protected jsLibUrl = 'http://localhost:8080/JSLib/rest';  // URL to web api !! NO '/' at the end of string

  constructor(protected bottomSheet: MatBottomSheet) {}

  protected openBottomSheet(data: any): void {
    this.bottomSheet.open(BottomSheetComponent, data);
  }

  protected handleSuccess(serviceSuccessMessage: string, result?: any): void {
    this.openBottomSheet({data: {
        info: serviceSuccessMessage,
        obj: !!result ? result : {}
      }});
  }

  protected handleError<T> (serviceErrMessage = 'Ошибка', result?: T) {
    return (error: any): Observable<T> => {
      this.openBottomSheet({data: {
          error: {message: serviceErrMessage + ` (${error.status})`}
        }});

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  protected getRightWord(num: number): string {
    let result: string;
    const tmpStr: string = '' + num;
    const val: number = +tmpStr.charAt(tmpStr.length - 1);
    switch (val) {
      case 1:
        result = ' запись';
        break;
      case 2:
      case 3:
      case 4:
        result = ' записи';
        break;
      default:
        result = ' записей';
    }
     return result;
    }
  }


