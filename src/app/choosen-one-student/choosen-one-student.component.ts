import {Component, Input, OnInit} from '@angular/core';
import {Student} from '../Entity/student';
import {Book} from '../Entity/book';
import {IssueService} from '../issue.service';
import {Issue} from '../Entity/issue';
import {formatDate} from '@angular/common';
import {Router} from '@angular/router';

@Component({
  selector: 'app-choosen-one-student',
  templateUrl: './choosen-one-student.component.html',
  styleUrls: ['./choosen-one-student.component.css']
})
export class ChoosenOneStudentComponent implements OnInit {
  private static student: Student;
  private static books: Book[];
  _isVisible: boolean;
  _isStudentPresent: boolean;
  _isBookVisible: boolean;

  constructor(private issueService: IssueService, private router: Router) {
  }

  ngOnInit() {
    if (!ChoosenOneStudentComponent.books) {
      ChoosenOneStudentComponent.books = [];
    }
    this.setVisible();
  }

  setVisible() {
    this._isStudentPresent = !!ChoosenOneStudentComponent.student;
    this._isBookVisible = !!ChoosenOneStudentComponent.books.length;
    this._isVisible = this._isStudentPresent || this._isBookVisible;
  }

  isButtonDisabled(): boolean {
    return !(this._isStudentPresent && this._isBookVisible);
  }

  setChoosenStudent(student: Student) {
    ChoosenOneStudentComponent.student = student;
    this.setVisible();
  }

  removeChoosenStudent() {
    ChoosenOneStudentComponent.student = null;
    this.setVisible();
  }

  getChoosenStudent(): Student {
    return ChoosenOneStudentComponent.student;
  }

  isVisible(): boolean {
    return this._isVisible;
  }

  getStudentAsString(): string {
    if (!this._isStudentPresent) {
      return 'Ученик не выбран';
    }
    return ChoosenOneStudentComponent.student.firstName
      + ' ' + ChoosenOneStudentComponent.student.lastName
      + ' ' + ChoosenOneStudentComponent.student.grade.grade;
  }

  addBook(book: Book): void {
    ChoosenOneStudentComponent.books.push(book);
    this.setVisible();
  }

  getBooks(): Book[] {
    return ChoosenOneStudentComponent.books;
  }

  removeBook(i: number): void {
    ChoosenOneStudentComponent.books.splice(i, 1);
    this.setVisible();
  }

  isTheBookInList(book: Book): boolean {
    for (const bookInList of ChoosenOneStudentComponent.books ) {
      if (book.id === bookInList.id) {
        return true;
      }
    }
    return false;
  }

  getBookAsString(book: Book): string {
     return book.bookName
      + ' ' + book.author.firstName
      + ' ' + book.author.lastName
      + ' ' + book.genre.genre
      + ' ' + book.country.country;
  }

  issueBooks(): void {
    const issues = [];
    for (const book of ChoosenOneStudentComponent.books) {
        issues.push({
          studentId: ChoosenOneStudentComponent.student.id,
          bookId: book.id
        });
    }
    this.issueService.addIssues(issues).subscribe(receivedIssues => {
      if (!!receivedIssues.length) {
        ChoosenOneStudentComponent.books = [];
        ChoosenOneStudentComponent.student = null;
        this.setVisible();
        this.router.navigate(['bookList']);
      }
    });
  }
}


