import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChoosenOneStudentComponent } from './choosen-one-student.component';

describe('ChoosenOneStudentComponent', () => {
  let component: ChoosenOneStudentComponent;
  let fixture: ComponentFixture<ChoosenOneStudentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChoosenOneStudentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChoosenOneStudentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
