import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {BookDetailComponent} from './book-detail/book-detail.component';
import {BookListComponent} from './book-list/book-list.component';
import {StudentListComponent} from './student-list/student-list.component';
import {StudentFormComponent} from './student-form/student-form.component';
import {BookFormComponent} from './book-form/book-form.component';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {StudentDetailComponent} from './student-detail/student-detail.component';

const appRoutes: Routes = [
  {
    path: 'bookDetail/:id', // bookInfo
    component: BookDetailComponent
  },
  {
    path: 'studentDetail/:id', // studentInfo
    component: StudentDetailComponent
  },
  {
    path: 'studentDetail', // studentInfo
    component: StudentDetailComponent
  },
  {
    path: 'studentList',
    component: StudentListComponent,
    runGuardsAndResolvers: 'always',
    data: { title: 'Список учеников' }
   },
  {
    path: 'bookList',
    component: BookListComponent,
    runGuardsAndResolvers: 'always',
    data: { title: 'Список книг'}
  },
  {
    path: 'addBook',
    component: BookFormComponent,
    data: { title: 'Добавить книгу', mode: 'add' }
  },
  {
    path: 'findBook',
    component: BookFormComponent,
    data: { title: 'Найти книгу', mode: 'find' }
  },
  {
    path: 'addStudent',
    component: StudentFormComponent,
    data: { title: 'Добавить ученика', mode: 'add' }
  },
  {
    path: 'findStudent',
    component: StudentFormComponent,
    data: { title: 'Найти ученика', mode: 'find' }
  },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  exports: [ RouterModule ],
  imports: [
    CommonModule,
    RouterModule.forRoot(appRoutes, { onSameUrlNavigation: 'reload'})
  ],
  declarations: []
})
export class AppRoutingModule {
  public static getRoutings(): Routes {
    return appRoutes;
  }
}
