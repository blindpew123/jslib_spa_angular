import {Book} from './book';
import {Student} from './student';

export class Issue {
  id: number;
  book: number;
  student: Student;
  issueDateTime: string;
  returnDateTime: string;
}
