import {Grade} from './grade';
import {Issue} from './issue';

export class Student {
  id: number;
  firstName: string;
  lastName: string;
  grade: Grade;
  studentIssues: Issue[];
}
