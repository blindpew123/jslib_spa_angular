import {Author} from './author';
import {Genre} from './genre';
import {Country} from './country';
import {Issue} from './issue';


export class Book {
  id: number;
  bookName: string;
  author: Author;
  genre: Genre;
  country: Country;
  bookIssues: Issue[];
}
