export class Author {
  id: number;
  lastName: string;
  firstName: string;
}
