import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, NavigationExtras, Router} from '@angular/router';
import {BookService} from '../book.service';
import {Book} from '../Entity/book';
import {FormState} from '../form-state';
import {MultiPurposeForm} from '../multi-purpose-form';
import {MatBottomSheet} from '@angular/material';
import {NgForm} from '@angular/forms';


@Component({
  selector: 'app-book-form',
  templateUrl: './book-form.component.html',
  styleUrls: ['./book-form.component.css']
})
export class BookFormComponent extends MultiPurposeForm implements OnInit {
  @ViewChild('bookForm') public bookForm: NgForm;
  book: Book;

  constructor(route: ActivatedRoute, router: Router, private bookService: BookService, bottomSheet: MatBottomSheet) {
    super(route, router, bottomSheet);
  }

  ngOnInit() {
    this.setFormState();
    this.book = this.bookService.getEmptyBook();
  }

  onSubmit(): void {
    if (this.state === FormState.Add) {
      this.bookService.addBook(this.book).subscribe(book => {
        this.book = this.bookService.getEmptyBook();
        this.bookForm.reset();
      });
      // redirect
    } else {
      this.bookService.findBooks(this.book).subscribe(findedBooks => {
        const navigationExtras: NavigationExtras = {
          queryParams: {
            'books': JSON.stringify(findedBooks)
          }
        };
        this.router.navigate(['bookList'], navigationExtras);
      });
    }
  }
}
