import { BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
// import {MatBottomSheetModule, MatListModule, MatProgressSpinnerModule, MatExpansionModule, MatButtonModule} from '@angular/material';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { AppRoutingModule } from './/app-routing.module';
import { BookListComponent } from './book-list/book-list.component';
import { BookDetailComponent } from './book-detail/book-detail.component';
import { StudentListComponent } from './student-list/student-list.component';
import { StudentDetailComponent } from './student-detail/student-detail.component';
import { BookFormComponent } from './book-form/book-form.component';
import { StudentFormComponent } from './student-form/student-form.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import {BottomSheetComponent} from './bottom-sheet/bottom-sheet.component';
import { ChoosenOneStudentComponent } from './choosen-one-student/choosen-one-student.component';
import {MaterialModule} from './material/material.module';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    BookListComponent,
    BookDetailComponent,
    StudentListComponent,
    StudentDetailComponent,
    BookFormComponent,
    StudentFormComponent,
    PageNotFoundComponent,
    BottomSheetComponent,
    ChoosenOneStudentComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
   ],
  providers: [],
  entryComponents: [
    BottomSheetComponent
  ],

  bootstrap: [AppComponent]
})
export class AppModule { }
