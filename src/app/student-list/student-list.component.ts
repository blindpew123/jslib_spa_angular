import {Component, OnDestroy, OnInit} from '@angular/core';
import {Student} from '../Entity/student';
import {StudentService} from '../student.service';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';

@Component({
  selector: 'app-student-list',
  templateUrl: './student-list.component.html',
  styleUrls: ['./student-list.component.css']
})
export class StudentListComponent implements OnInit, OnDestroy {

  students: Student[];
  navigationSubscription;
  constructor(private studentService: StudentService, private route: ActivatedRoute, private router: Router) {
    this.navigationSubscription = this.router.events.subscribe((e: any) => {
      // If it is a NavigationEnd event re-initialise the component
      if (e instanceof NavigationEnd) {
        this.initializeList();
      }
    });
  }

  ngOnInit() {}

  ngOnDestroy(): void {
    // avoid memory leaks here by cleaning up after ourselves. If we
    // don't then we will continue to run our initialiseInvites()
    // method on every navigationEnd event.
    if (this.navigationSubscription) {
      this.navigationSubscription.unsubscribe();
    }
  }
  private initializeList(): void {
    this.route.queryParams.subscribe(params => {
      if (!!params['students']) {
        this.students = JSON.parse(params['students']);
      } else {
        this.getStudents();
      }
    });
  }


  getStudents(): void {
    this.studentService.getStudents().subscribe(students => this.students = students);
  }

}
