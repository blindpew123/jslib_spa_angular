import {Component, OnInit, ViewChild} from '@angular/core';
import {FormState} from '../form-state';
import {Student} from '../Entity/student';
import {ActivatedRoute, NavigationExtras, Router} from '@angular/router';
import {StudentService} from '../student.service';
import {MultiPurposeForm} from '../multi-purpose-form';
import {MatBottomSheet} from '@angular/material';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-student-form',
  templateUrl: './student-form.component.html',
  styleUrls: ['./student-form.component.css']
})

export class StudentFormComponent extends MultiPurposeForm implements OnInit {
  @ViewChild('studentForm') public studentForm: NgForm;
  student: Student;
  private studentService: StudentService;

  constructor(route: ActivatedRoute, router: Router, studentService: StudentService, bottomSheet: MatBottomSheet) {
    super(route, router, bottomSheet);
    this.studentService = studentService;
  }

  ngOnInit() {
    this.setFormState();
    this.student = this.studentService.getEmptyStudent();
  }
  onSubmit(): void {
     if (this.state === FormState.Add) {
      this.studentService.addStudent(this.student).subscribe(student => {
        if (!!student) {
          this.student = this.studentService.getEmptyStudent();
          this.studentForm.reset();
        }
      });
    } else {
      this.studentService.findStudents(this.student).subscribe(findedStudents => {
        const navigationExtras: NavigationExtras = {
          queryParams: {
            'students': JSON.stringify(findedStudents)
          }
        };
        this.router.navigate(['studentList'], navigationExtras);
      });
    }
  }

}
